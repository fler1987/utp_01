package com.utp.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.utp.model.Nota;
import com.utp.repo.NotaRepo;

@Service
public class NotaService {

	@Autowired
	NotaRepo notaRepo;
	
	public ArrayList<Nota> listarNotas(){
		return (ArrayList<Nota>)notaRepo.findAll();
	}
	
	public Nota crearNota(Nota nota) {
		return notaRepo.save(nota);
	}
	
}
