package com.utp.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.utp.model.Nota;
import com.utp.services.NotaService;

@RestController
@RequestMapping("/nota")
public class NotaController {

	@Autowired
	NotaService notaService;
	
	@GetMapping()
	public ArrayList<Nota> listarNotas(){
		return notaService.listarNotas();
	}
	
	@PostMapping()
	public Nota crearNota(@RequestBody Nota nota) {
		String usuario = printUser(SecurityContextHolder.getContext().getAuthentication());
		nota.setUsuario(usuario);
		return notaService.crearNota(nota);
	}
	 
	private String printUser(Authentication authentication) {
	    authentication.getAuthorities().forEach(a -> a.getAuthority());
	    //logger.info(authentication.getName());
	    return authentication.getName();
	}
	
}
