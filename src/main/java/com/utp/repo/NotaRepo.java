package com.utp.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.utp.model.Nota;

@Repository
public interface NotaRepo extends CrudRepository<Nota, Integer> {

}
