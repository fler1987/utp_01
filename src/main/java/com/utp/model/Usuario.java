package com.utp.model;

import javax.persistence.Entity;
import javax.persistence.*;

@Entity
@Table(name = "usuario")
public class Usuario {

	@Id
	@Column(unique = true, nullable = false)
	private Integer idUsuario;
	private String nombre;
	private String clave;
	private String rol;
	private Boolean estado;	
	
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	public Boolean getEstado() {
		return estado;
	}
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
}
